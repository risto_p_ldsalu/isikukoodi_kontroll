#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <ctime>


using namespace std;


bool is_digits(const std::string &str)
{
    return str.find_first_not_of("0123456789") == std::string::npos;
}

int checkid(string id) {
    stringstream convert;
    int error = 0;

    if (id.length() != 11) {
        return 1;
    }

    if (!is_digits(id)) {
        return 2;
    }

    std::string century = id.substr(0,1);
    std::string year = id.substr(1,2);
    std::string month = id.substr(3,2);
    std::string day = id.substr(5,2);
    std::string place = id.substr(7,3);
    std::string control = id.substr(10);

    int c = 0;
    stringstream convertFirst(century);
    convertFirst >> c;

    int l = 0;
    stringstream convertSecond(place);
    convertSecond >> l;

    int cr = 0;
    stringstream convertThird(control);
    convertThird >> cr;

    if (!(c <= 8 && c >= 1)) {
        error = error + 4;
    }

    int y = 0;
    stringstream convertForth(year);
    convertForth >> y;

    std::string yearFull = "";
    switch(c){
        case 1 :

        case 2 :
            yearFull = "18";
            yearFull.append(year);

            break;
        case 3 :

        case 4 :
            yearFull = "19";
            yearFull.append(year);

            break;
        case 5 :

        case 6:
            yearFull = "20";
            yearFull.append(year);

            break;
        case 7 :

        case 8 :
            yearFull = "21";
            yearFull.append(year);

            break;
    }

    struct tm tm;
    std::string dt = "";
    dt.append(day);
    dt.append("/");
    dt.append(month);
    dt.append("/");
    dt.append(yearFull);

    //kuu kontroll
    int m = 0;
    stringstream convertFifth(month);
    convertFifth >> m;
    if (!(m > 0 && m <=12)) {
        error = error + 8;
    }

    //p�eva kontroll
    int d = 0;
    stringstream convertSixth(day);
    convertSixth >> d;

    int yearInt = 0;
    stringstream convertSeventh(yearFull);
    convertSeventh >> yearInt;

    int numberOfDays = 0;
    if (m == 4 || m == 6 || m == 9 || m == 11) {
        numberOfDays = 30;
    } else if (m == 2) {
        bool isLeapYear = (yearInt % 4 == 0 && yearInt % 100 != 0) || (yearInt % 400 == 0);
        if (isLeapYear) {
            numberOfDays = 29;
        } else {
            numberOfDays = 28;
        }
    } else {
        numberOfDays = 31;
    }
    if (d > numberOfDays) {
        error = error + 16;
    }

/*
    if (strptime(dt.c_str(), "%d/%m/%Y", &tm)) {
        std::cout << "date is valid" << std::endl;
    }
    else {
        std::cout << "date is invalid" << std::endl;
    }
    */
    int left_over = 0;
    int divided = 0;
    int nr = 0;
    for (int a = 0; a < 10; a= a+1) {
        std::string string = id.substr(a,1);
        int b = 0;
        stringstream convertEight(id.substr(a,1));
        convertEight >> b;
        if (a == 9) {
            nr = nr + 1*b;
        } else {
            nr = nr + (a+1)*b;
        }
    }
    divided = nr/11;
    left_over = nr - 11*divided;
    std::cout << left_over << std::endl;

    int nr2 = 0;
    if (left_over == 10) {
        int weigth = 3;
        for (int a = 0; a < 10; a = a + 1) {
            std::string string = id.substr(a,1);
            int b = 0;
            stringstream convertNine(id.substr(a,1));
            convertNine >> b;
            nr2 = nr2 + weigth*b;
            if (weigth == 9) {
                weigth = 0;
            }

            weigth++;
        }
        divided = nr/11;
        left_over = nr - 11*divided;
    }

    int b = 0;
    stringstream convertTen(id.substr(10,1));
    convertTen >> b;
    if (left_over != b) {
        error = error + 32;
    }


    std::cout << left_over << std::endl;

    return error;
}

int main()
{
    cout << checkid("39304146534") << endl;
    return 0;
}

